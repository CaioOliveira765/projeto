package br.com.ozcorp;

public enum TipoSanguineo {
	A_POSITIVO("A+"),A_NEGATIVO("A-"),AB_POSITIVO("AB+"),B_POSITIVO("B+"),B_NEGATIVO("B-"),AB_NEGATIVO("AB-"),O_POSITIVO("O+"),O_NEGATIVO("O-");
	
	public String titulo;
	
	private TipoSanguineo(String titulo) {
		this.titulo = titulo;
	}
}

