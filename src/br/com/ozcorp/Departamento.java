package br.com.ozcorp;

public class Departamento {
	private String nome;
	private String sigla;
	private Cargo cargo;
	
	public Departamento(String nome, String sigla, Cargo cargo) {
		this.nome = nome;
		this.sigla = sigla;
		this.cargo = cargo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
}	
