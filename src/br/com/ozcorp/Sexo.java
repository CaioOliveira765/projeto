package br.com.ozcorp;

public enum Sexo {
	MASCULINO("MASCULINO"),
	FEMININO("FEMININO"),
	OUTRO("OUTRO");
	
	public String titulo;
	
	private Sexo(String titulo) {
		this.titulo = titulo;
	}
	}

