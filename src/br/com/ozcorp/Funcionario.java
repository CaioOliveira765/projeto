package br.com.ozcorp;

public class Funcionario {
	protected String nome;
	protected String rg;
	protected String cpf;
	protected String matricula;
	protected String email;
	protected String senha;
	protected Cargo cargo;
	protected TipoSanguineo tipoSanguineo;
	protected Sexo sexo;
	protected int nivelAcesso;
	protected Departamento departamento;
	
	
	
	
	public Funcionario(String nome, String rg, String cpf, String matricula, String email, String senha, Cargo cargo,
			TipoSanguineo tipoSanguineo, Sexo sexo, int nivelAcesso, Departamento departamento) {
		super();
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.email = email;
		this.senha = senha;
		this.cargo = cargo;
		this.tipoSanguineo = tipoSanguineo;
		this.sexo = sexo;
		this.nivelAcesso = nivelAcesso;
		this.departamento = departamento;
	}

	//geteer e setters
	public String getNome() {
		return nome;
	}




	public void setNome(String nome) {
		this.nome = nome;
	}




	public String getRg() {
		return rg;
	}




	public void setRg(String rg) {
		this.rg = rg;
	}




	public String getCpf() {
		return cpf;
	}




	public void setCpf(String cpf) {
		this.cpf = cpf;
	}




	public String getMatricula() {
		return matricula;
	}




	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public String getSenha() {
		return senha;
	}




	public void setSenha(String senha) {
		this.senha = senha;
	}




	public Cargo getCargo() {
		return cargo;
	}




	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}




	public TipoSanguineo getTipoSanguineo() {
		return tipoSanguineo;
	}




	public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}




	public Sexo getSexo() {
		return sexo;
	}




	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}




	public int getNivelAcesso() {
		return nivelAcesso;
	}




	public void setNivelAcesso(int nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}




	public Departamento getDepartamento() {
		return departamento;
	}




	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
}
