package br.com.ozcorp;

public class Main {

	public static void main(String[] args) {
		
		Diretor diretor = new Diretor("Carlinhos", "148782158", "015487.024", "1478548", "Carlinhos.sort@gmail.com", "123", new Cargo("Diretor de Ti", 5.000), TipoSanguineo.A_POSITIVO, Sexo.MASCULINO, 0, new Departamento("Tecnico em informatica", "TI", new Cargo("Diretor Ti", 5.000)));
		
		System.out.println("Nome: " + diretor.getNome());
		System.out.println("RG: " + diretor.getRg());
		System.out.println("CPF: " + diretor.getCpf());
		System.out.println("Matricula" + diretor.getMatricula());
		System.out.println("Email: " + diretor.getEmail()) ;
		System.out.println("Senha: " + diretor.getSenha());
		System.out.println("Tipo Sanguineo: " + diretor.getTipoSanguineo().titulo);
		System.out.println("Sexo: " + diretor.getSexo().titulo);
		System.out.println("Departamento: " + diretor.getDepartamento().getNome());
		System.out.println("Cargo: " + diretor.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario: " + diretor.getDepartamento().getCargo().getSalarioBase());
	}
	
		Analista analista = new Analista("Paula", "15481517", "14461478.021", "1842151", "Paulinha@gmail.com", "123", new Cargo("Analista", 5000), TipoSanguineo.AB_POSITIVO, Sexo.FEMININO, 0, new Departamento("Analista de Sistema", "Anals",new Cargo("Analista de Sistema", 5000) ));
		
		System.outprintln("Nome: " + analista.getNome());
		System.out.println("RG: " + analista.getRg());
		System.out.println("CPF: " + analista.getCpf());
		System.out.println("Matricula" + analista.getMatricula());
		System.out.println("Email: " + analista.getEmail()) ;
		System.out.println("Senha: " + analista.getSenha());
		System.out.println("Tipo Sanguineo: " + analista.getTipoSanguineo().titulo);
		System.out.println("Sexo: " + analista.getSexo().titulo);
		System.out.println("Departamento: " + analista.getDepartamento().getNome());
		System.out.println("Cargo: " + analista.getDepartamento().getCargo().getTitulo());
		System.out.println("Salario: " + analista.getDepartamento().getCargo().getSalarioBase());
		
		
}
